package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.example.ConstantsTest.EXPECTED_MAP;
import static org.example.ConstantsTest.INPUT_LIST;

/**
 * @author Deissy Coral
 */
class PalindromeTest {
    @Test
    void shouldBePalindrome() {
        boolean result = Palindrome.isPalindrome(ConstantsTest.PALINDROME_STRING);
        Assertions.assertTrue(result);
    }

    @Test
    void shouldNotBePalindrome() {
        boolean result = Palindrome.isPalindrome(ConstantsTest.NOT_PALINDROME_STRING);
        Assertions.assertFalse(result);
    }

    @Test
    void getAPalindromeList() {
        Map<String, Boolean> result = Palindrome.validatePalindrome(INPUT_LIST);
        Assertions.assertEquals(EXPECTED_MAP, result);
    }

    @Test
    void getAPalindromeListWhenIsEmpty() {
        Map<String, Boolean> result = Palindrome.validatePalindrome(List.of());
        Assertions.assertTrue(result.isEmpty());
    }

    @Test
    void getAPalindromeListWhenIsNull() {
        Map<String, Boolean> result = Palindrome.validatePalindrome(null);
        Assertions.assertTrue(result.isEmpty());
    }
}
