package org.example;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author Deissy Coral
 */
class NumbersWithoutLoopsTest {

    @ParameterizedTest
    @ValueSource(ints = {0, 1, 5, 10})
    void shouldReturnAnArray(int count) {
        int[] result = NumbersWithoutLoops.init(count);
        for (int i = 0; i < count; i++) {
            Assertions.assertEquals(i, result[i]);
        }

    }

}