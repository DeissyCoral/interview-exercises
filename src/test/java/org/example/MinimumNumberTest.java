package org.example;

import static org.example.ConstantsTest.EXPECTED_INTEGER_MAP;
import static org.example.ConstantsTest.INPUT_INTEGER_LIST;
import static org.example.ConstantsTest.NEGATIVE_NUMBER;
import static org.example.ConstantsTest.NEGATIVE_NUMBER_EXPECTED;
import static org.example.ConstantsTest.POSITIVE_NUMBER;
import static org.example.ConstantsTest.POSITIVE_NUMBER_EXPECTED;

import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * @author Deissy Coral
 */
class MinimumNumberTest {

  @Test
  void orderPositiveNumber() {
    Integer result = MinimumNumber.getMinimumNumber(POSITIVE_NUMBER);
    Assertions.assertEquals(POSITIVE_NUMBER_EXPECTED, result);
  }

  @Test
  void orderNegativeNumber() {
    Integer result = MinimumNumber.getMinimumNumber(NEGATIVE_NUMBER);
    Assertions.assertEquals(NEGATIVE_NUMBER_EXPECTED, result);
  }

  @ParameterizedTest
  @ValueSource(ints = {0, -0, -0000, -0000})
  void orderZeroNumber(Integer value) {
    Integer result = MinimumNumber.getMinimumNumber(value);
    Assertions.assertEquals(value, result);
  }

  @Test
  void orderAlistOfNumbers() {
    Map<Integer, Integer> result = MinimumNumber.validateNumber(INPUT_INTEGER_LIST);
    result
        .keySet()
        .forEach(key -> Assertions.assertEquals(EXPECTED_INTEGER_MAP.get(key), (result.get(key))));
  }

  @Test
  void getANumberListWhenIsEmpty() {
    Map<Integer, Integer> result = MinimumNumber.validateNumber(List.of());
    Assertions.assertTrue(result.isEmpty());
  }

  @Test
  void getANumberListWhenIsNull() {
    Map<Integer, Integer> result = MinimumNumber.validateNumber(null);
    Assertions.assertTrue(result.isEmpty());
  }
}
