package org.example;

import java.util.List;
import java.util.Map;

/**
 * @author Deissy Coral
 */
public class ConstantsTest {
  public static String PALINDROME_STRING = "Anita Lava la Tina ";
  public static String NOT_PALINDROME_STRING = "Anita y La Tina ";
  public static List<String> INPUT_LIST =
      List.of(
          "anita lava la tina",
          "ABC_CBA",
          "Anita lavaba la tina",
          "123454321",
          "ABCBA",
          "ABCCBA",
          "Definitely no",
          "Unonu unonU");
  public static Map<String, Boolean> EXPECTED_MAP =
      Map.of(
          "anita lava la tina",
          true,
          "ABC_CBA",
          true,
          "Anita lavaba la tina",
          false,
          "123454321",
          true,
          "ABCBA",
          true,
          "ABCCBA",
          true,
          "Definitely no",
          false,
          "Unonu unonU",
          true);
  public static Integer NEGATIVE_NUMBER = -5109035;
  public static Integer NEGATIVE_NUMBER_EXPECTED = -9553100;
  public static Integer POSITIVE_NUMBER = 5109035;
  public static Integer POSITIVE_NUMBER_EXPECTED = 1003559;
  public static List<Integer> INPUT_INTEGER_LIST =
      List.of(3501, -367, 7080420, 60084, 59901, -20800030, 707821701, 100101);
  public static Map<Integer, Integer> EXPECTED_INTEGER_MAP =
      Map.of(3501, 1035,
              -367, -763,
              7080420, 2000478,
              60084, 40068,
              59901, 10599,
              -20800030, -83200000,
              707821701, 100127778,
              100101, 100011);
}
