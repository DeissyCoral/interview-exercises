package org.example;

/**
 * @author Deissy Coral
 */
public class Constants {
    public static final String ZERO = "0";
    public static final String NEGATIVE_SIGNAL = "-";
}
