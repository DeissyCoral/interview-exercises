package org.example;

import static org.example.Constants.NEGATIVE_SIGNAL;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Deissy Coral
 * Minimum number: Find the minimum possible number you can build with the
 *     digits of each number. The number should not begin with 0. Also, respect the sign of the
 *     number
 *     <p>Input:
 *     [3501, -367, 7080420, 60084, 59901, -20800030, 707821701, 100101]
 *
 *     <p>Expected output:
 *     The minimum number with 3501 is: 1035
 *     The minimum number with -367 is: -367 // -763
 *     The minimum number with 7080420 is: 2000478
 *     The minimum number with 60084 is: 40068
 *     The minimum number with 59901 is: 10599
 *     The minimum number with -20800030 is: -20000038 // -83200000
 *     The minimum number with 707821701 is: 100127778
 *     The minimum number with 100101 is: 100011
 */
public class MinimumNumber {
  public static Map<Integer, Integer> validateNumber(List<Integer> inputList) {
    return Optional.ofNullable(inputList).stream()
        .flatMap(Collection::stream)
        .collect(Collectors.toMap(value -> value, MinimumNumber::getMinimumNumber));
  }

  public static Integer getMinimumNumber(Integer input) {
    List<String> digits = Arrays.asList(String.valueOf(input).split(""));
    String firstValue = NEGATIVE_SIGNAL;
    if (input >= 0) {
      digits = digits.stream().sorted().collect(Collectors.toCollection(ArrayList::new));
      firstValue = digits.stream().filter(d -> !d.equals(Constants.ZERO)).findFirst().orElse(Constants.ZERO);
      digits.remove(firstValue);
    } else {
      digits =
          digits.stream()
              .filter(d -> !d.equals(NEGATIVE_SIGNAL))
              .sorted(Comparator.reverseOrder())
              .collect(Collectors.toCollection(ArrayList::new));
    }
    digits.add(0, firstValue);
    return Integer.parseInt(String.join("", digits));
  }
}
