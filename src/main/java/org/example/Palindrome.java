package org.example;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Deissy Coral
 *     <p>Palindrome: Find if each element from the list is a Palindrome (ignore whitespace and
 *     case). Return the result as a map (key-value array) where the key is the element and the
 *     value a boolean indicating whether the element is or is not a palindrome.
 *     <p>Input:
 *     ["anita lava la tina",
 *     "ABC_CBA",
 *     "Anita lavaba la tina",
 *     "123454321",
 *     "ABCBA",
 *     "ABCCBA",
 *     "Definitely no",
 *     "Unonu unonU"]
 *
 *     <p>Expected output:
 *     {123454321=true,
 *     ABCBA=true,
 *     ABC_CBA=true,
 *     Anita lavaba la tina=false,
 *     Unonu unonU=true,
 *     anita lava la tina=true,
 *     ABCCBA=true,
 *     Definitely no=false}
 */
public class Palindrome {
  public static Map<String, Boolean> validatePalindrome(List<String> inputList) {
    return Optional.ofNullable(inputList).stream()
        .flatMap(Collection::stream)
        .collect(Collectors.toMap(value -> value, Palindrome::isPalindrome));
  }

  public static boolean isPalindrome(String input) {
    String inputWithoutSpaces = input.replace(" ", "");
    return new StringBuilder(inputWithoutSpaces)
        .reverse()
        .toString()
        .equalsIgnoreCase(inputWithoutSpaces);
  }
}
