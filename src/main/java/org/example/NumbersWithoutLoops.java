package org.example;

/**
 * @author Deissy Coral
 * Print a sequence of numbers without using loops (for/while)
 **/
public class NumbersWithoutLoops {
    public static int[] init(int count) {
        int value = 0;
        int[] values = new int[count];
        return process(count, value, values);
    }

    private static int[] process(int count, int value, int[] values) {
        if (count > 0) {
            values[value] = value;
            count--;
            value++;
            process(count, value, values);
        }
        return values;
    }
}
